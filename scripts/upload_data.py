import argparse
import os
import re
import shutil
import subprocess
from datetime import datetime
from pathlib import Path

import pandas as pd
from google.api_core.exceptions import InvalidArgument
from google.cloud import aiplatform
from loguru import logger

DATA_FOLDER = "data"
PATTERNS = ("*.jpg", "*.JPG", "*.png", "*.PNG")


if __name__ == "__main__":
    # Parse the arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--data-version",
        type=str,
        required=False,
        default="",
        help="version of the data to be uploaded",
    )
    args = parser.parse_args()

    # Define the data version
    data_version = None
    if args.data_version and args.data_version != "":
        # If a correct timestamp is provided, use it as the data version number
        if re.match(r"\d{14}", args.data_version):
            data_version = args.data_version
            logger.info(f"Provided data version: {data_version}.")
        else:
            logger.warning(
                "Argument `data-version` is in the wrong format. It will be ignored."
            )
    if data_version is None:
        # Set the version number of the data asset to the current UTC time
        logger.info("Data version not provided, setting to current timestamp...")
        data_version = datetime.now().strftime("%Y%m%d%H%M%S")

    # Get project info and set dataset name
    project_name = os.environ.get("VERTEX_PROJECT_ID")
    project_location = os.environ.get("VERTEX_LOCATION")
    dataset_name = f"agricultural-pests-{data_version}"

    # Set GCS upload path
    gcs_path = f"{os.environ.get('PIPELINE_FILES_GCS_PATH')}/data/{data_version}"

    # Extract file names and labels
    data_path = Path(DATA_FOLDER).absolute()
    files = [str(f) for p in PATTERNS for f in data_path.glob(f"**/{p}")]
    labels = [f.rsplit("/", 2)[1] for f in files]
    logger.info("Extracted file paths and labels.")

    # Create file to label mappings for both local and GCS files
    df = pd.DataFrame(columns=["FILE_PATH", "LABEL"])
    df["FILE_PATH"] = files
    df["LABEL"] = labels
    df.to_csv(f"{DATA_FOLDER}/local_mapping.csv", index=False)

    df_gcs = pd.DataFrame(columns=["GCS_FILE_PATH", "LABEL"])
    gcs_files = [f"{gcs_path}/{'/'.join(f.rsplit('/', 2)[-2:])}" for f in files]
    df_gcs["GCS_FILE_PATH"] = gcs_files
    df_gcs["LABEL"] = labels
    df_gcs.to_csv(f"{DATA_FOLDER}/gcs_mapping.csv", index=False)

    # Check if this data version already exists
    gsutil_path = shutil.which("gsutil")
    check_cmd = [gsutil_path, "-q", "stat", f"{gcs_path}/gcs_mapping.csv"]
    check_status = subprocess.run(check_cmd)

    # If the data already exists, skip upload
    if check_status.returncode == 0:
        logger.warning(f"GCS path {gcs_path} is already populated. Skipping upload.")
    else:
        copy_cmd = [
            gsutil_path,
            "-m",
            "cp",
            "-r",
            str(Path(DATA_FOLDER).absolute()),
            gcs_path,
        ]
        subprocess.run(copy_cmd, check=True)
        logger.info(f"Uploaded data to GCS path {gcs_path}.")

    # If the dataset already exists, skip creation
    try:
        dataset = aiplatform.ImageDataset(
            dataset_name=dataset_name,
            project=project_name,
            location=project_location,
        )
        logger.warning("Dataset {dataset_name} already exists, skipping creation.")
    except InvalidArgument:
        logger.info(
            f"Creating dataset {dataset_name} in project {project_name}, "
            f"location {project_location}..."
        )
        dataset = aiplatform.ImageDataset.create(
            display_name=dataset_name,
            gcs_source=f"{gcs_path}/gcs_mapping.csv",
            project=project_name,
            location=project_location,
            import_schema_uri=(
                aiplatform.schema.dataset.ioformat.image.single_label_classification
            ),
            labels=dict(
                task_name="agricultural-pests-classification",
                data_version=data_version,
            ),
        )
        dataset.wait()
        logger.info(f"Created managed dataset {dataset_name}.")
