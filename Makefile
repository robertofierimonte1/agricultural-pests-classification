#!make
.envvars: set-current-env-vars
set-current-env-vars:
	@poetry run python -m src.utils.environment

-include set-current-env-vars
-include .env
enable-caching ?= ""
data-version ?= ""
export

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

pre-commit: ## Run the pre-commit over the entire repo
	@poetry run pre-commit run --all-files

pre-commit-light: ## Run the pre-commit over the entire repo skipping the notebooks stripout hook
	@export SKIP=nbstripout && $(MAKE) pre-commit

download-data: ## Download the dataset inside the `data` folder for local usage (called automatically from inside `make setup`)
	@poetry run python -m scripts.download_data && \
		mv data/agricultural-pests-image-dataset/* data/ && \
		rm -rf data/agricultural-pests-image-dataset/

setup: ## Install all the required Python dependencies, download the data, and create a jupyter kernel for the project
	@poetry install && \
		$(MAKE) download-data && \
		poetry run python -m ipykernel install --user --name="agricultural-pests-venv"

upload-data: ## Upload the data from the local folder to GCS and create a managed dataset. Optionally specify data-version={data_version}
	@poetry run python -m scripts.upload_data --data-version ${data-version}
