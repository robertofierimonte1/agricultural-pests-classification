import os

import tensorflow as tf
from keras.layers import AveragePooling2D, Conv2D, Dense, Flatten
from loguru import logger

from src.base.model import InputBlock


class LeNet5(tf.keras.Model):
    """LeNet5 CNN model."""

    def __init__(self, width: int, height: int, n_classes: int, *args, **kwargs):
        """Initialise all the layers in the model.

        Args:
            width (int): Width of the input image
            height (int): Height of the input image
            n_classes (int): Number of classes (neurons in the last layer)
        """
        super().__init__(*args, **kwargs)
        self._width = width
        self._height = height

        self.input_block = InputBlock(width, height)
        self.conv2d_1 = Conv2D(filters=6, kernel_size=(3, 3), activation="relu")
        self.maxpool2d_1 = AveragePooling2D(pool_size=(2, 2))
        self.conv2d_2 = Conv2D(filters=16, kernel_size=(3, 3), activation="relu")
        self.flatten = Flatten()
        self.dense1 = Dense(units=120, activation="relu")
        self.dense2 = Dense(units=n_classes, activation="softmax")

        logger.debug("Initialised model.")

    def call(self, inputs: tf.Tensor) -> tf.Tensor:
        """Call the model on some inputs and return the output.

        Args:
            inputs (Tensor): Inputs to the model

        Returns:
            Tensor: Output of the model given the inputs
        """
        x = self.input_block(inputs)
        x = self.conv2d_1(x)
        x = self.maxpool2d_1(x)
        x = self.conv2d_2(x)
        x = self.flatten(x)
        x = self.dense1(x)
        x = self.dense2(x)
        return x

    def predict_class(self, x: tf.Tensor) -> tf.Tensor:
        """Predict the most likely class given the input images.

        Args:
            x (Tensor): Inputs to the model

        Returns:
            Tensor: Most likely class (as an integer)
                for each input
        """
        return tf.math.argmax(self.predict(x), axis=1)

    @staticmethod
    def load_model(file_name: os.PathLike) -> tf.keras.Model:
        """Load the pre-trained model from a source.

        Args:
            file_name (os.PathLike): Path from where the model will be loaded.

        Returns:
            keras.Model: The pre-trained model.
        """
        model = tf.keras.models.load_model(file_name)
        logger.info(f"Loaded model from {file_name}.")
        return model

    def save_model(self, file_name: os.PathLike) -> None:
        """Save the pre-trained model to a target.

        Args:
            file_name (os.PathLike): Path where the model will be saved to.
        """
        directory = os.path.join(file_name, os.path.pardir)
        os.makedirs(directory, exist_ok=True)
        self.save(file_name)
        logger.info(f"Saved model to {file_name}.")
