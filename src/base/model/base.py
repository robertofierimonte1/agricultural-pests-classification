import os

import tensorflow as tf
from keras.layers import (
    Conv2D,
    Dense,
    Flatten,
    InputLayer,
    Layer,
    MaxPooling2D,
    RandomContrast,
    RandomFlip,
    RandomRotation,
    RandomZoom,
    Rescaling,
    Resizing,
)
from loguru import logger


class InputBlock(Layer):
    def __init__(self, width: int, height: int, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._width = width
        self._height = height

        self._input = InputLayer(input_shape=[None], dtype=tf.float64, ragged=True)
        self.random_flip = RandomFlip(seed=42)
        self.random_rotation = RandomRotation(factor=0.1, seed=42)
        self.random_zoom = RandomZoom(height_factor=0.1, seed=42)
        self.random_contrast = RandomContrast(factor=0.1, seed=42)

    def call(self, inputs: tf.Tensor, training: bool = False) -> tf.Tensor:
        x = self._input(inputs)
        x = Resizing(width=self._width, height=self._height)(x)
        x = Rescaling(scale=1.0 / 255)(x)
        if training is True:
            x = self.random_flip(x)
            x = self.random_rotation(x)
            x = self.random_zoom(x)
            x = self.random_contrast(x)
        return x


class BaseCNNModel(tf.keras.Model):
    """Simple CNN model."""

    def __init__(self, width: int, height: int, n_classes: int, *args, **kwargs):
        """Initialise all the layers in the model.

        Args:
            width (int): Width of the input image after resizing
            height (int): Height of the input image after resizing
        """
        super().__init__(*args, **kwargs)

        self.input_block = InputBlock(width=width, height=height)
        self.conv2d_1 = Conv2D(filters=35, kernel_size=(3, 3), activation="relu")
        self.maxpool2d_1 = MaxPooling2D(pool_size=(2, 2))
        self.conv2d_2 = Conv2D(filters=64, kernel_size=(3, 3), activation="relu")
        self.maxpool2d_2 = MaxPooling2D(pool_size=(2, 2))
        self.conv2d_3 = Conv2D(filters=64, kernel_size=(3, 3), activation="relu")

        self.flatten = Flatten()
        self.dense1 = Dense(units=64, activation="relu")
        self.dense2 = Dense(units=n_classes, activation="softmax")

        logger.debug("Initialised model.")

    def call(self, inputs: tf.Tensor) -> tf.Tensor:
        """Call the model on some inputs and return the output.

        Args:
            inputs (tf.Tensor): Inputs to the model

        Returns:
            tf.Tensor: Output of the model given the inputs
        """
        x = self.input_block(inputs)
        x = self.conv2d_1(x)
        x = self.maxpool2d_1(x)
        x = self.conv2d_2(x)
        x = self.maxpool2d_2(x)
        x = self.conv2d_3(x)
        x = self.flatten(x)
        x = self.dense1(x)
        x = self.dense2(x)
        return x

    def predict_class(self, x: tf.Tensor) -> tf.Tensor:
        """Predict the most likely class given the input images.

        Args:
            x (tf.Tensor): Inputs to the model

        Returns:
            tf.Tensor: Most likely class (as an integer)
                for each input
        """
        return tf.math.argmax(self.predict(x), axis=1)

    @staticmethod
    def load_model(file_name: os.PathLike) -> tf.keras.Model:
        """Load the pre-trained model from a source.

        Args:
            file_name (os.PathLike): Path from where the model will be loaded.

        Returns:
            tf.keras.Model: The pre-trained model.
        """
        model = tf.keras.models.load_model(file_name)
        logger.info(f"Loaded model from {file_name}.")
        return model

    def save_model(self, file_name: os.PathLike) -> None:
        """Save the pre-trained model to a target.

        Args:
            file_name (os.PathLike): Path where the model will be saved to.
        """
        directory = os.path.join(file_name, os.path.pardir)
        os.makedirs(directory, exist_ok=True)
        self.save(file_name)
        logger.info(f"Saved model to {file_name}.")
