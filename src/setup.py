from setuptools import find_packages, setup

from src.__version__ import version

setup(
    name="agricultural-pests-classification",
    version=version,
    packages=find_packages(),
    install_requires=(),
    include_package_data=True,
    description="Deep Learning model to classify agricultural pests.",
)
